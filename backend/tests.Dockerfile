# Container for running all the automated tests
FROM stragivari/php-scheduler:7.4_1
# Mount the following files:
# /app/src - all your PHP sources here
# /app/tests - all Codeception tests here
# /app/codeception.yml - Codeception config file
# /app/.env - Environment variables

CMD /app/vendor/bin/codecept bootstrap /app/codeception.yml && /app/vendor/bin/codecept run --steps
