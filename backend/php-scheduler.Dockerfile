# Task scheduler, with pre-installed PHP
# Mount the following files:
# /etc/cron.d/custom-cron-tasks -> crontab-file containing the tasks to schedule
# /app - all your PHP sources here

FROM ubuntu:20.04

# Install necessary packages
RUN apt update
RUN apt install --no-install-recommends --no-install-suggests -y \
		cron php7.4-cli mysql-client zip unzip composer less
RUN apt install --no-install-recommends --no-install-suggests -y \
		php7.4-curl php7.4-mysql php7.4-bcmath php7.4-dom php7.4-zip

# Cleanup
RUN rm -rf /var/lib/apt/lists/*

# Prepare app infrastructure
RUN mkdir /app
WORKDIR /app
COPY composer.json /app
COPY composer.lock /app
RUN composer install
COPY php.ini /etc/php/7.4/cli/php.ini

# Run the task scheduler
CMD chmod 0644 /etc/cron.d/custom-cron-tasks && crontab /etc/cron.d/custom-cron-tasks && cron -f