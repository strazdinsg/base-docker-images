#!/bin/sh

# A script that run liquibase DB sync script with correct parameters, depending on whether we want to sync PROD or TEST DB
# Also cleans all the data for test DB
# Environment variables must be defined: ENV ("prod" or "test") MYSQL_USER, MYSQL_PWD, MYSQL_HOST, MYSQL_PORT, MYSQL_DB

if [ "$ENV" = "prod" ]
then
  echo "Synchronizing PROD database..."
  java -cp "lib/*" liquibase.integration.commandline.Main --contexts=prod --driver=org.mariadb.jdbc.Driver --username="$MYSQL_USER" --password="$MYSQL_PWD" --url=jdbc:mysql://"$MYSQL_HOST":"$MYSQL_PORT"/"$MYSQL_DB" --changeLogFile=db-changelogs.yml update
  echo "PROD DB sync DONE"
elif [ "$ENV" = "test" ]
then
  echo "Synchronizing TEST database..."
  echo "Deleting existing TEST DB..."
  mysql -u "$MYSQL_USER" --password="$MYSQL_PWD" -h "$MYSQL_HOST" -P "$MYSQL_PORT" -e "DROP DATABASE $MYSQL_DB;"
  echo "DONE Deleting. Creating new clean Test DB..."
  mysql -u "$MYSQL_USER" --password="$MYSQL_PWD" -h "$MYSQL_HOST" -P "$MYSQL_PORT" -e "CREATE DATABASE $MYSQL_DB;"
  echo "DONE. Creating structure and importing test data..."
  java -cp "lib/*" liquibase.integration.commandline.Main --contexts=test --driver=org.mariadb.jdbc.Driver --username="$MYSQL_USER" --password="$MYSQL_PWD" --url=jdbc:mysql://"$MYSQL_HOST":"$MYSQL_PORT"/"$MYSQL_DB" --changeLogFile=db-changelogs.yml update
  echo "TEST DB sync DONE"
else
  echo "Wrong DB-sync environment specified!"
  exit 1
fi