# A tool container for synchronizing (migrating) database structure. 
FROM openjdk:11.0.8-jre-slim
# Mount the following files:
# /liquibase/db-changelogs.yml -> a file where all the Liquibase changelogs are included

RUN apt update && apt install -y --no-install-recommends --no-install-suggests default-mysql-client mysql-common mariadb-common

# Copy our Liquibase libraries and DB setup scripts into the app directory
RUN mkdir /liquibase
WORKDIR /liquibase
ADD ./db-versioning /liquibase
RUN chmod a+x /liquibase/dbsync.bash

CMD /liquibase/dbsync.bash