# PHP backend - a REST web server
FROM devwithlando/php:7.4-apache-2
# Mount the following files:
# /app - all your PHP sources here
# /var/www/html - index.php and the rest of website files here

RUN a2enmod rewrite
RUN mkdir /app

WORKDIR /app

COPY composer.json /app
COPY composer.lock /app
COPY php.ini /usr/local/etc/php

RUN composer install
